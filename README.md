# SIE_project_storm



## Getting started

Since there are already the csv files, you can import them on MySQL.
Once thats done, create a new project on Storm, take the directory "core" and the
file "project.xml" from the gitlab and subtitute these elements in your Storm project's file.

Login to Storm (name: Storm; no password)
Establish a connection between MySQL and Storm following the login elements present in the file "dataset.xml".

Then your good to fully explore my BI solution on Storm (Don't forget to load the data, because there is no auto-refresh).